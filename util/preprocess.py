#!/usr/bin/python3

# Preprocessing script for the car data.
# Run as
#   python3 preprocess.py car.data car.ppdata car.attrs
# where
#   car.data is the input file with original data
#   car.ppdata is the output file where preprocessed data will be written
#   car.attrs is the output file where attribute information will be written

import sys

# Map attribute values to numbers for easier handling
mappings = [
    { "low": 1, "med": 2, "high": 3, "vhigh": 4 },
    { "low": 1, "med": 2, "high": 3, "vhigh": 4 },
    { "2": 1, "3": 2, "4": 3, "5more": 4 },
    { "2": 1, "4": 2, "more": 3 },
    { "small": 1, "med": 2, "big": 3 },
    { "low": 1, "med": 2, "high": 3 },
    { "unacc": 1, "acc": 2, "good": 3, "vgood": 4 },
]

# Read input data
def read_data(infile):
    data = []
    with open(infile) as f:
        for line in f:
            arr = line.strip().split(',')
            if len(arr) != 7:
                raise RuntimeError("Case contains unexpected number of attributes")
            data.append(arr)
    return data

def preprocess(infile, ppfile, attrfile):
    data = read_data(infile)
    ppdata = []
    for case in data:
        ppcase = []
        for i in range(0, 7):
            ppcase.append(mappings[i][case[i]])
        ppdata.append(ppcase)
    # Write preprocessed data
    with open(ppfile, 'w') as f:
        for case in ppdata:
            f.write("{}\n".format(','.join(map(str, case))))
    # Write attribute values (ugly hardcoding...)
    with open(attrfile, 'w') as f:
        f.write("buying_price:low,med,high,vhigh\n")
        f.write("maintenance_cost:low,med,high,vhigh\n")
        f.write("doors:2,3,4,5more\n")
        f.write("persons:2,4,more\n")
        f.write("lug_boot:small,med,big\n")
        f.write("safety:low,med,high\n")
        f.write("class:unacc,acc,good,vgood\n")

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise RuntimeError("Input file required!")
    elif len(sys.argv) < 4:
        raise RuntimeError("Output files required!")
    preprocess(sys.argv[1], sys.argv[2], sys.argv[3])
