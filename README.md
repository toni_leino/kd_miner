This is a simple rule-based classifier. It works, but it's slow and probably
not very good. 

To run the classifier you must have Python 3 installed. You can then use the
following command:

    python3 src/main.py data/car.data data/car.attr

There are some parameters in `src/main.py` that you can try to change.

`example_results.txt` contains output from one run of the classifier. Scroll
to the bottom to see how well the classifier works.

The dataset was created by Marko Bohanec and it is freely available
[here](https://archive.ics.uci.edu/ml/datasets/Car+Evaluation).

The classifier, which includes all the Python files under `src` and `util`,
is copyright (C) Toni Leino 2017. It's free/libre software and you can use it
under the terms of [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
