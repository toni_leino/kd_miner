# Represents a single rule.

from copy import deepcopy

class Rule:
    # Initializes a rule with empty LHS and given RHS.
    # class_attr is the attribute that this rule tries to classify
    def __init__(self, class_attr, rhs):
        # LHS is a dictionary of sets where keys are attributes and the
        # corresponding set contains possible values for that attribute.
        self.lhs = {}
        self.class_attr = class_attr
        self.rhs = rhs

    # Adds a condition where attr==value
    def add_lhs(self, attr, value):
        if attr not in self.lhs:
            self.lhs[attr] = set()
        self.lhs[attr].add(value)

    # Returns true if this rule applies to the given case
    def matches(self, case):
        for cond in self.lhs.items():
            attr = cond[0]
            values = cond[1]
            if case[attr] not in values:
                return False
        return True

    # Returns false if rule classifies the given case correctly
    def is_accurate(self, case):
        return case[self.class_attr] == self.rhs

    # Returns string representation of the rule.
    def __str__(self):
        # If LHS is empty, assume that this is the default rule.
        if len(self.lhs) == 0:
            return "default => {}={}".format(self.class_attr, self.rhs)
        else:
            # Should return something like
            #   foo={1,2} and bar=3 => class=abcd
            buf = []
            for elem in self.lhs.items():
                buf.append("{}={}".format(elem[0], self._format_value_set(elem[0], elem[1])))
            return "{} => {}={}".format(" and ".join(buf), self.class_attr, self.rhs)

    # Returns a string representation of a set of attribute values.
    # (Implementation detail, please ignore)
    def _format_value_set(self, attr, value_set):
        buf = []
        for elem in value_set:
            buf.append(elem)
        length = len(value_set)
        if length == 0:
            return "{}"
        elif length == 1:
            return buf[0]
        else:
            return "\{{}\}".format(",".join(buf))

    # https://stackoverflow.com/questions/1500718/what-is-the-right-way-to-override-the-copy-deepcopy-operations-on-an-object-in-p
    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            setattr(result, k, deepcopy(v, memo))
        return result
