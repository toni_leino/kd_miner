# Dataset. Contains the actual data and attribute information.

class Dataset:
    def __init__(self, data, attr_info):
        # Transform arrays into dicts
        self.data = []
        for item in data:
            d = {}
            for i in range(len(item)):
                attr = attr_info[i]
                d[attr["name"]] = item[i]
            self.data.append(d)
        self.attr_info = attr_info
