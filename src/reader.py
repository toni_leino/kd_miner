# Functions for reading datasets and attribute info

def read_data(filename):
    data = []
    num_attrs = None
    with open(filename) as f:
        for line in f:
            arr = line.strip().split(',')
            if not num_attrs:
                num_attrs = len(arr)
            if len(arr) != num_attrs:
                raise RuntimeError("All cases should have the same number of attributes!")
            data.append(arr)
    return data

def read_attribute_info(filename):
    attr_info = []
    with open(filename) as f:
        for line in f:
            arr = line.strip().split(':')
            if len(arr) != 2:
                raise RuntimeError("Attribute info should contain name and possible values")
            attr_name = arr[0]
            attr_values = arr[1].split(',')
            # There should be at least one possible values for each attribute
            # (though in real life data there should probably be at least two)
            if len(attr_values) < 1:
                raise RuntimeError("Attribute {} has no values!".format(attr_name))
            attr_info.append({ "name": attr_name, "values": attr_values })
    return attr_info
