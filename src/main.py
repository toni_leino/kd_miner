# Frontend for the rule generator

import copy
import math
import random
import sys

import reader

from dataset import Dataset
from rule import Rule

#
# Parameters! You can change these
#

# Number of folds for k-fold cross-evaluation
NUM_FOLDS = 10

# Print stats for each fold
FOLDWISE_STATS = False

# Print rules generated for each fold
FOLDWISE_RULES = False

# Minimum accuracy for a rule
MIN_ACCURACY = 0.66

#
# Parameters end here
#

# Counts how many times each different attribute value appears in the data.
def count_frequencies(data, attr):
    counts = {}
    for item in data:
        value = item[attr]
        if value not in counts:
            counts[value] = 0
        counts[value] += 1
    return counts

# Mines rules from the dataset. Rules are generated for the given class
# variable.
def mine_rules(dataset, class_attr):
    # Count how many times each class appears in the dataset
    counts = count_frequencies(dataset.data, class_attr)

    # Sort classes by frequency in ascending order
    sorted_classes = []
    while len(counts) > 0:
        min_freq = len(dataset.data)
        min_class = None
        for elem in counts.items():
            if elem[1] < min_freq:
                min_class = elem[0]
                min_freq = elem[1]
        sorted_classes.append(min_class)
        del counts[min_class]

    # Mine rules
    rules = []
    unclassified_cases = dataset.data.copy()
    target_class = 0
    default_class = len(sorted_classes)-1
    # This list should contain attributes that can be used for classification
    attrs = dataset.attr_info.copy()
    attrs = list(filter(lambda x: x["name"] != class_attr, attrs))

    while target_class != default_class:
        rule = mine_one_rule(unclassified_cases, attrs, class_attr, sorted_classes[target_class])
        # If the new rule is good enough, add it to the rule list and remove
        # matching cases.
        if calculate_accuracy(unclassified_cases, rule) > MIN_ACCURACY:
            rules.append(rule)
            unclassified_cases = list(filter(lambda x: not rule.matches(x), unclassified_cases))
        # Otherwise begin making rules for the next class.
        else:
            target_class += 1

    # Add default rule as the last rule
    default_rule = Rule(class_attr, sorted_classes[default_class])
    rules.append(default_rule)
    return rules

# Mines the best possible rule for the given target class.
def mine_one_rule(data, attrs, class_attr, target_class):
    unused_attrs = attrs.copy()
    rule = Rule(class_attr, target_class)
    return grow_rule(data, attrs, rule)

# Grow rule recursively as much as possible
def grow_rule(data, attrs, rule):
    # No attributes left, cannot grow rule anymore
    if len(attrs) == 0:
        return rule

    # Find best possible { attribute, value } combination and add it to the rule
    best_attr = None
    best_value = None
    best_accuracy = None
    best_rule = None
    for attr in attrs:
        attr_name = attr["name"]
        for value in attr["values"]:
            new_rule = copy.deepcopy(rule)
            new_rule.add_lhs(attr_name, value)
            accuracy = calculate_accuracy(data, new_rule)
            if best_accuracy == None or accuracy > best_accuracy:
                best_attr = attr_name
                best_value = value
                best_accuracy = accuracy
                best_rule = new_rule

    # If the best rule is still worse than the original rule, return the
    # original rule.
    if best_accuracy <= calculate_accuracy(data, rule):
        return rule

    # Otherwise try to grow the rule further.
    unused_attrs = list(filter(lambda x: x["name"] != best_attr, attrs))
    return grow_rule(data, unused_attrs, best_rule)

# Calculates accuracy of a rule using Laplace ratio
def calculate_accuracy(data, rule):
    num_covered = 0
    num_correct = 0
    for case in data:
        if rule.matches(case):
            num_covered += 1
            if rule.is_accurate(case):
                num_correct += 1
    return (num_correct + 1) / (num_covered + 2)

# Creates folds for k-fold cross-validation
def make_folds(data, k=10):
    # Shuffle data
    # sample() is used instead of shuffle() because it automatically makes
    # a copy
    shuffled = random.sample(data, len(data))

    folds = []
    for i in range(k):
        folds.append([])
    curr_fold = 0
    # This way the last fold isn't noticeably smaller than the others if
    # len(data) is not divisible by k
    for elem in shuffled:
        folds[curr_fold].append(elem)
        curr_fold += 1
        if curr_fold == k:
            curr_fold = 0
    return folds

# Evaluates accuracy of generated rules
def evaluate_rules(test_set, class_attr, rules):
    num_cases = len(test_set.data)
    num_correct = 0
    classwise = {}
    for attr in test_set.attr_info:
        if attr["name"] == class_attr:
            for value in attr["values"]:
                classwise[value] = { "total": 0, "correct": 0 }
    for case in test_set.data:
        for rule in rules:
            if rule.matches(case):
                classwise[rule.rhs]["total"] += 1
                if rule.rhs == case[rule.class_attr]:
                    num_correct += 1
                    classwise[rule.rhs]["correct"] += 1
                break
    if FOLDWISE_STATS:
        freqs = count_frequencies(test_set.data, class_attr)
        print("{}/{} classified correctly with {} rules".format(num_correct, num_cases, len(rules)))
        print("Classwise accuracies:")
        for k, v in classwise.items():
            print("  {}: {}/{} correctly classified ({} cases in test set)".format(k, v["correct"], v["total"], freqs[k]))
    if FOLDWISE_RULES:
        print("Rules generated:")
        for rule in rules:
            print("  {}".format(str(rule)))
    return num_correct, classwise

if __name__ == "__main__":
    if len(sys.argv) < 3:
        raise RuntimeError("Dataset and attribute info files required!")
    data = reader.read_data(sys.argv[1])
    attr_info = reader.read_attribute_info(sys.argv[2])
    print("Read {} cases with {} attributes.".format(len(data), len(attr_info)))
    class_attr = "class"

    # k-fold cross-validation
    folds = make_folds(data, NUM_FOLDS)
    total_correct = 0
    total_classwise = {}
    for i in range(len(folds)):
        test_set = folds[i]
        training_set = []
        for j in range(len(folds)):
            if j != i:
                training_set += folds[j]
        rules = mine_rules(Dataset(training_set, attr_info), class_attr)
        if FOLDWISE_STATS or FOLDWISE_RULES:
            print("Fold {}:".format(i))
        num_correct, classwise = evaluate_rules(Dataset(test_set, attr_info), class_attr, rules)
        total_correct += num_correct
        for k, v in classwise.items():
            if k not in total_classwise:
                total_classwise[k] = { "total": 0, "correct": 0 }
            total_classwise[k]["total"] += v["total"]
            total_classwise[k]["correct"] += v["correct"]

    # Statistics
    print("=== Final results ===")
    print("{}/{} cases classified correctly ({:.1f}%)".format(total_correct, len(data), (total_correct/len(data)*100)))
    print("Classwise statistics:")
    freqs = count_frequencies(Dataset(data, attr_info).data, class_attr)
    for k, v in total_classwise.items():
        percentage = 0.0
        if v["total"] > 0:
            percentage = v["correct"]/v["total"] * 100.0
        print("  {}: {}/{} correctly classified ({:.1f}%) ({} cases in dataset)".format(k, v["correct"], v["total"], percentage, freqs[k]))
